package unrn.edu.ar.docker.mappers;

import org.mapstruct.*;
import unrn.edu.ar.docker.dto.DepartmentDTO;
import unrn.edu.ar.docker.dto.EmployeeDTO;
import unrn.edu.ar.docker.model.Department;
import unrn.edu.ar.docker.model.Employee;

import java.util.ArrayList;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = EmployeeMapper.class)
public interface DepartmentMapper {

    @Mapping(target = "employees", ignore = true)
    DepartmentDTO toDTO(Department entity);

    @Mapping(target = "employees", ignore = true)
    DepartmentDTO toDTOList(Department entity, boolean ignore);

    @Mapping(target = "employeesId", ignore = true)
    Department toEntity(DepartmentDTO dto);

    void mapToEntity(DepartmentDTO dto, @MappingTarget Department entity);

    @AfterMapping
    default void setEmployees(@MappingTarget DepartmentDTO departmentDTO, Department department) {
        departmentDTO.setEmployees(new ArrayList<>());
        department.getEmployeesId().forEach(employee ->
                departmentDTO.getEmployees().add(new EmployeeDTO(employee)));
    }

    @AfterMapping
    default void setEmployees(@MappingTarget Department department, DepartmentDTO departmentDTO) {
        department.setEmployeesId(departmentDTO.getEmployees()
                .stream().map(EmployeeDTO::getId).collect(Collectors.toList()));
    }
}
